//
//  main.m
//  Exercise02
//
//  Created by Bryan Lor on 9/11/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSLAppDelegate class]));
    }
}
