//
//  BSLAppDelegate.h
//  Exercise02
//
//  Created by Bryan Lor on 9/11/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
