//
//  BSLCollectionViewController.m
//  Exercise02
//
//  Created by Bryan Lor on 9/11/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "BSLCollectionViewController.h"
#import "BSLNumberLayout.h"
#import "BSLNumberCell.h"

static NSString * const numberCellIdentifier = @"BSLNumberCell";

@interface BSLCollectionViewController ()

@property (nonatomic, weak) IBOutlet BSLNumberLayout *numberLayout;

@end

@implementation BSLCollectionViewController

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout{
    
    self = [super initWithCollectionViewLayout:layout];
    
    if (self) {
        UINavigationItem *collectionViewTitle = self.navigationItem;
        
        collectionViewTitle.title = @"Number Cell Collection";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
    
    [self.collectionView registerClass:[BSLNumberCell class]
            forCellWithReuseIdentifier:numberCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 10;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BSLNumberCell *numberCell = [collectionView dequeueReusableCellWithReuseIdentifier:numberCellIdentifier
                                                                          forIndexPath:indexPath];
    
    return numberCell;
}

@end
