//
//  BSLNumberCell.m
//  Exercise02
//
//  Created by Bryan Lor on 9/11/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "BSLNumberCell.h"

@implementation BSLNumberCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.95f alpha:1.0f];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
