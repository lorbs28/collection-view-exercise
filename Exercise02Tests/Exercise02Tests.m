//
//  Exercise02Tests.m
//  Exercise02Tests
//
//  Created by Bryan Lor on 9/11/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Exercise02Tests : XCTestCase

@end

@implementation Exercise02Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
